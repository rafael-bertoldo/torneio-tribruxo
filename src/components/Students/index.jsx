import { useState } from "react";
import Card from "../Card";
import "./style.css";

function Students({
  setIsShow,
  isShow,
  gryffindorStudents,
  slytherinStudents,
  hufflepuffStudents,
  ravenclawStudents,
}) {
  function getRandom(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  function getNumberofElts(arr) {
    return arr.length - 1;
  }

  function getRandomStudent(arr, getRandom, getNumberofElts) {
    return arr[getRandom(0, getNumberofElts(arr))];
  }

  const gryffindorWizard = getRandomStudent(
    gryffindorStudents,
    getRandom,
    getNumberofElts
  );

  const slytherinWizard = getRandomStudent(
    slytherinStudents,
    getRandom,
    getNumberofElts
  );

  const hufflepuffWizard = getRandomStudent(
    hufflepuffStudents,
    getRandom,
    getNumberofElts
  );

  const ravenclawWizard = getRandomStudent(
    ravenclawStudents,
    getRandom,
    getNumberofElts
  );

  const fourWizards = [
    gryffindorWizard,
    slytherinWizard,
    hufflepuffWizard,
    ravenclawWizard,
  ];
  const firstWizard = getRandomStudent(fourWizards, getRandom, getNumberofElts);

  const [secondWizard, setSecondWizard] = useState(
    getRandomStudent(fourWizards, getRandom, getNumberofElts)
  );

  const [thirdWizard, setThirdWizard] = useState(
    getRandomStudent(fourWizards, getRandom, getNumberofElts)
  );

  return (
    <section className="students__main">
      <div className="students__main-container">
        <div className="students__container">
          <h2 className="students__title">Wizard 1:</h2>
          <Card
            key={firstWizard.name}
            name={firstWizard.name}
            image={firstWizard.image}
            gender={firstWizard.gender}
            house={firstWizard.house}
          />
        </div>
        <div className="students__container">
          <h2 className="students__title">Wizard 2:</h2>
          {secondWizard !== firstWizard &&
          secondWizard.house !== firstWizard.house ? (
            <Card
              key={secondWizard.name}
              name={secondWizard.name}
              image={secondWizard.image}
              gender={secondWizard.gender}
              house={secondWizard.house}
            />
          ) : (
            setSecondWizard(
              getRandomStudent(fourWizards, getRandom, getNumberofElts)
            )
          )}
        </div>
        <div className="students__container">
          <h2 className="students__title">Wizard 3:</h2>
          {thirdWizard !== secondWizard &&
          thirdWizard !== firstWizard &&
          thirdWizard.house !== secondWizard.house &&
          thirdWizard.house !== firstWizard.house ? (
            <Card
              key={thirdWizard.name}
              name={thirdWizard.name}
              image={thirdWizard.image}
              gender={thirdWizard.gender}
              house={thirdWizard.house}
            />
          ) : (
            setThirdWizard(
              getRandomStudent(fourWizards, getRandom, getNumberofElts)
            )
          )}
        </div>
      </div>

      <button className="btn" onClick={() => setIsShow(!isShow)}>
        Return to home
      </button>
    </section>
  );
}

export default Students;
