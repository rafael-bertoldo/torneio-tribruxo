import "./style.css";

function Description({ isShow, setIsShow }) {
  return (
    <section className="description__main">
      <div className="description__main-context">
        <h2 className="description__main-context-title">Torneio Tribruxo!</h2>
        <p className="description__main-context-paragraph">
          "A Glória eterna! Isto é o que aguarda o estudante que vencer o
          torneio tribruxo, mas para isso, ele precisará sobreviver em três
          tarefas. Três tarefas extremamente perigosas." — Alvo Dumbledore
          apresentando o torneioHarry Potter e o Cálice de Fogo (filme)
        </p>
        <p className="description__main-context-paragraph">
          O torneio tribruxo normalmente é realizado entre as três maiores
          escolas de magia e bruxaria, porém neste ano o diretor de Hogwarts
          resolveu executar um pequeno torneio com apenas os alunos de sua
          escola!
        </p>
        <p className="description__main-context-paragraph">
          Então vamos fazer a vez do cálice de fogo e sortear quem serão os 3
          bruxos que buscaram a GLÓRIA ETERNA aqui em Hogwarts!
        </p>
      </div>
      <button className="btn" onClick={() => setIsShow(!isShow)}>
        sortear participantes
      </button>
    </section>
  );
}

export default Description;
