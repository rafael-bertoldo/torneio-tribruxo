import "./style.css";
import { FaLinkedin, FaGithub } from "react-icons/fa";

function Footer() {
  return (
    <footer>
      powered by R.J. Bertoldo
      <a
        href="https://www.linkedin.com/in/rafaeljbertoldo/"
        rel="noreferrer"
        target="_blank"
      >
        <FaLinkedin color={"blue"} size={30} />
      </a>
      <a href="https://github.com/jagochitz" rel="noreferrer" target="_blank">
        <FaGithub color={"black"} size={30} />
      </a>
    </footer>
  );
}

export default Footer;
