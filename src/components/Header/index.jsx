import "./style.css";
import logo from "./logo.png";

function Header() {
  return (
    <header className="header__main">
      <figure>
        <img src={logo} alt="logo" className="header__logo-image" />
        <figcaption className="hidden">Hogwarts Logo</figcaption>
      </figure>
    </header>
  );
}

export default Header;
