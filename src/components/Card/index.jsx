import "./style.css";

function Card({ name, image, gender, house, id }) {
  return (
    <div className="card__main">
      <div className="card__main-text-content">
        <h2>Name: {name}</h2>
        <h2>Gender: {gender}</h2>
        <h2>House: {house}</h2>
      </div>
      <img className="card__img" src={image} alt="stdImg" />
    </div>
  );
}

export default Card;
