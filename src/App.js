import "./App.css";
import Header from "./components/Header";
import Description from "./components/Description";
import Students from "./components/Students";
import Footer from "./components/Footer";
import { useEffect, useState } from "react";

function App() {
  const [isShow, setIsShow] = useState(true);
  const [studentList, setStudentList] = useState([]);

  useEffect(() => {
    fetch("https://hp-api.herokuapp.com/api/characters/students")
      .then((response) => response.json())
      .then((response) => setStudentList(response))
      .catch((err) => console.log(err));
  }, []);

  const gryffindorStudents = studentList.filter(
    (item) => item.house === "Gryffindor"
  );
  const slytherinStudents = studentList.filter(
    (item) => item.house === "Slytherin"
  );
  const hufflepuffStudents = studentList.filter(
    (item) => item.house === "Hufflepuff"
  );
  const ravenclawStudents = studentList.filter(
    (item) => item.house === "Ravenclaw"
  );

  return (
    <div className="App">
      <Header />
      <div className="container">
        <main className="main">
          {isShow ? (
            <Description setIsShow={setIsShow} isShow={isShow} />
          ) : (
            <Students
              gryffindorStudents={gryffindorStudents}
              slytherinStudents={slytherinStudents}
              hufflepuffStudents={hufflepuffStudents}
              ravenclawStudents={ravenclawStudents}
              studentList={studentList}
              setIsShow={setIsShow}
              isShow={isShow}
            />
          )}
        </main>
        <Footer />
      </div>
    </div>
  );
}

export default App;
